package com.greenbox.notes;

import com.greenbox.notes.presentation.presenter.AddPresenterImplTest;
import com.greenbox.notes.presentation.presenter.MainPresenterImplTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/09/16
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({AddPresenterImplTest.class, MainPresenterImplTest.class})
public class UnitTestsSuit {
}
