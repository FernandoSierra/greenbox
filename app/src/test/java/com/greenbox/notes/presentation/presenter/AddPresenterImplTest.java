package com.greenbox.notes.presentation.presenter;

import com.greenbox.notes.database.controller.NoteController;
import com.greenbox.notes.model.Note;
import com.greenbox.notes.presentation.interactor.AddViewInteractor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 22/07/16
 */
public class AddPresenterImplTest {
    private static final int TEST_ID = 100;
    @Mock
    private AddViewInteractor mAddViewInteractor;
    @Mock
    private NoteController mNoteController;
    private AddPresenterImpl mAddPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mAddPresenter = new AddPresenterImpl(mAddViewInteractor, mNoteController);
    }

    @Test
    public void saveOrUpdateNote_noteNull_callExtractViewDataToNoteWithZero() {
        mAddPresenter.saveOrUpdateNote(null);
        verify(mAddViewInteractor, times(1)).extractViewDataToNote(0);
    }

    @Test
    public void saveOrUpdateNote_noteNotNull_callExtractViewDataToNoteWithNoteId() {
        Note note = new Note();
        note.setId(TEST_ID);
        mAddPresenter.saveOrUpdateNote(note);
        verify(mAddViewInteractor, times(1)).extractViewDataToNote(TEST_ID);
    }

    @Test
    public void saveOrUpdateNote_filledNoteNull_notCallShowError() {
        when(mAddViewInteractor.extractViewDataToNote(anyLong())).thenReturn(null);
        mAddPresenter.saveOrUpdateNote(null);
        verify(mAddViewInteractor, times(0)).showError();
    }

    @Test
    public void saveOrUpdateNote_filledNoteWithAnyId_callUpdate() {
        Note note = new Note();
        note.setId(TEST_ID);
        Note filledNote = new Note();
        filledNote.setId(TEST_ID);
        when(mAddViewInteractor.extractViewDataToNote(TEST_ID)).thenReturn(filledNote);
        mAddPresenter.saveOrUpdateNote(note);
        verify(mNoteController, times(1)).update(filledNote, mAddPresenter);
    }

    @Test
    public void onRetrieveWithTrue_callClose() {
        mAddPresenter.onRetrieve(true);
        verify(mAddViewInteractor, times(1)).close();
    }

    @Test
    public void onRetrieveWithFalse_callShowError() {
        mAddPresenter.onRetrieve(false);
        verify(mAddViewInteractor, times(1)).showError();
    }

    @Test
    public void onError_callShowError() {
        mAddPresenter.onError(mock(Exception.class));
        verify(mAddViewInteractor, times(1)).showError();
    }
}