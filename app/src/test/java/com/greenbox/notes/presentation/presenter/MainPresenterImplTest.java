package com.greenbox.notes.presentation.presenter;

import android.support.annotation.NonNull;

import com.greenbox.notes.database.controller.NoteController;
import com.greenbox.notes.database.listener.OnRetrieveListener;
import com.greenbox.notes.model.Note;
import com.greenbox.notes.presentation.interactor.MainViewInteractor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 21/07/16
 */
public class MainPresenterImplTest {
    @Mock
    private MainViewInteractor mMainViewInteractor;
    @Mock
    private NoteController mNoteController;
    private MainPresenterImpl mMainPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mMainPresenter = new MainPresenterImpl(mMainViewInteractor, mNoteController);
    }

    @Test
    public void refreshData_callShowRefresh() throws Exception {
        mMainPresenter.refreshData();
        verify(mMainViewInteractor).showRefresh();
    }

    @Test
    public void refreshDataOnRetrieve_callUpdateAndHideRefresh() throws Exception {
        OnRetrieveListener onRetrieveListener = getOnRetrieveListenerForGetAll();
        List<Note> mockedList = getMockedNotes();
        onRetrieveListener.onRetrieve(mockedList);
        verify(mMainViewInteractor).update(mockedList);
        verify(mMainViewInteractor).hideRefresh();
    }

    @Test
    public void refreshDataOnError_callShowErrorAndHideRefresh() throws Exception {
        OnRetrieveListener onRetrieveListener = getOnRetrieveListenerForGetAll();
        onRetrieveListener.onError(mock(Exception.class));
        verify(mMainViewInteractor).showError();
        verify(mMainViewInteractor).hideRefresh();
    }

    @Test
    public void deleteNoteOnRetrieve_callRefreshData() throws Exception {
        mMainPresenter = spy(mMainPresenter);
        OnRetrieveListener onRetrieveListener = getOnRetrieveListenerForDeleteNote();
        onRetrieveListener.onRetrieve(true);
        verify(mMainPresenter).refreshData();
    }

    @Test
    public void deleteNoteOnRetrieve_callShowError() throws Exception {
        OnRetrieveListener onRetrieveListener = getOnRetrieveListenerForDeleteNote();
        onRetrieveListener.onRetrieve(false);
        verify(mMainViewInteractor).showError();
    }

    @Test
    public void deleteNoteOnError_callShowError() throws Exception {
        OnRetrieveListener onRetrieveListener = getOnRetrieveListenerForDeleteNote();
        onRetrieveListener.onError(mock(Exception.class));
        verify(mMainViewInteractor).showError();
    }

    @NonNull
    private List<Note> getMockedNotes() {
        List<Note> mockedList = new ArrayList<>();
        mockedList.add(mock(Note.class));
        return mockedList;
    }

    private OnRetrieveListener getOnRetrieveListenerForGetAll() {
        ArgumentCaptor<OnRetrieveListener> argumentCaptor = ArgumentCaptor.forClass(OnRetrieveListener.class);
        mMainPresenter.refreshData();
        verify(mNoteController).getAll(argumentCaptor.capture());
        return argumentCaptor.getValue();
    }

    private OnRetrieveListener getOnRetrieveListenerForDeleteNote() {
        ArgumentCaptor<OnRetrieveListener> argumentCaptor = ArgumentCaptor.forClass(OnRetrieveListener.class);
        mMainPresenter.deleteNote(0);
        verify(mNoteController).delete(anyLong(), argumentCaptor.capture());
        return argumentCaptor.getValue();
    }
}