package com.greenbox.notes.model;

import android.os.Parcel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 28/07/16
 */
public class NoteTest {
    private Note mNote;
    private Parcel mParcel;

    @Before
    public void setUp() throws Exception {
        mNote = new Note();
        mNote.setId(1);
        mNote.setMessage("Test Note");
        mNote.setDate(System.currentTimeMillis());

        mParcel = Parcel.obtain();
    }

    @After
    public void tearDown() throws Exception {
        mParcel.recycle();
    }

    @Test
    public void convertToParcelAndRecoverIt() throws Exception {
        mNote.writeToParcel(mParcel, 0);
        mParcel.setDataPosition(0);
        Note note = Note.CREATOR.createFromParcel(mParcel);
        assertTrue(mNote.equals(note));
    }
}