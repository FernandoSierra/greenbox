package com.greenbox.notes.database;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/09/16
 */

public class TestDatabase {
    private Context mContext;
    private DbHelper mDbHelper;

    public TestDatabase(@NonNull Context context) {
        mContext = context;
        mDbHelper = new DbHelper(context);
        mDbHelper.getWritableDatabase();
    }

    public DbHelper getDbHelper() {
        return mDbHelper;
    }

    public void clean() {
        mContext.deleteDatabase(mDbHelper.getDatabaseName());
    }
}
