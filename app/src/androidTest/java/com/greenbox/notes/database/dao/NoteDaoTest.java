package com.greenbox.notes.database.dao;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.greenbox.notes.database.TestDatabase;
import com.greenbox.notes.model.Note;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/09/16
 */
@RunWith(AndroidJUnit4.class)
public class NoteDaoTest {
    private static final long BASE_DATE = System.currentTimeMillis();
    private static final int TEST_ITEMS = 3;
    private static final String TEST_MESSAGE = "Test message ";
    private static final double DAY = 8.64e7;
    private NoteDao mNoteDao;
    private TestDatabase mTestDatabase;

    @Before
    public void setUp() throws Exception {
        mTestDatabase = new TestDatabase(InstrumentationRegistry.getTargetContext());
        mNoteDao = new NoteDao(mTestDatabase.getDbHelper());
    }

    @After
    public void tearDown() throws Exception {
        mTestDatabase.clean();
    }

    @Test
    public void getNotes_hasNoValues() throws Exception {
        List<Note> notes = mNoteDao.getNotes();
        assertNotNull(notes);
        assertEquals(0, notes.size());
    }

    @Test
    public void getNotes_hasValues() throws Exception {
        insertValidTestItems();
        List<Note> notes = mNoteDao.getNotes();
        assertNotNull(notes);
        assertEquals(TEST_ITEMS, notes.size());
    }

    @Test
    public void addNote() throws Exception {
        Note note = new Note();
        boolean wasAdded = mNoteDao.addNote(note);
        assertTrue(wasAdded);
    }

    private void insertValidTestItems() {
        for (int index = 0; index < TEST_ITEMS; index++) {
            Note testNote = new Note();
            testNote.setMessage(TEST_MESSAGE + index);
            testNote.setDate(BASE_DATE + (long) (index * DAY));
            mNoteDao.addNote(testNote);
        }
    }
}