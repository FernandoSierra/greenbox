package com.greenbox.notes;

import com.greenbox.notes.model.NoteTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/09/16
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({NoteTest.class})
public class InstrumentedSuite {
}
