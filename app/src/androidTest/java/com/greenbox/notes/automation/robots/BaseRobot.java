package com.greenbox.notes.automation.robots;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;

import com.greenbox.notes.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.greenbox.notes.automation.matchers.NotesMatcher.withToolbarTitle;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/11/16
 */

public abstract class BaseRobot {
    Context mContext;

    BaseRobot(@NonNull Context context) {
        mContext = context;
    }

    public BaseRobot back() {
        pressBack();
        return this;
    }

    public BaseRobot checkToolbarTitle(@NonNull String title) {
        onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(title)));
        return this;
    }

    public BaseRobot checkEmptyError() {
        onView(allOf(
                withId(android.support.design.R.id.snackbar_text),
                withText(R.string.empty_note_error)))
                .check(matches(isDisplayed()));
        return this;
    }

    public BaseRobot deleteNote() {
        onView(withText(R.string.delete_yes))
                .perform(click());
        return this;
    }

    public BaseRobot cancelDeleteNote() {
        onView(withText(R.string.common_cancel))
                .perform(click());
        return this;
    }

    public BaseRobot checkDeleteDialogIsPresent() {
        onView(withText(R.string.delete_note))
                .inRoot(isDialog())
                .check(matches(isDisplayed()));
        return this;
    }
}
