package com.greenbox.notes.automation;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.greenbox.notes.R;
import com.greenbox.notes.automation.robots.MainRobot;
import com.greenbox.notes.database.DbHelper;
import com.greenbox.notes.view.activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 19/10/16
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HomeTest {
    private static final int FIRST_NOTE_POSITION = 0;
    private static final long FIRST_NOTE_ID = 1;
    private final String[] mDummyNotes = new String[]{
            "First",
            "Second",
            "Third",
            "Fourth",
            "Fifth"
    };
    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);
    private Context mContext = InstrumentationRegistry.getTargetContext();
    private String mNewNoteTitle;
    private String mEditNoteTitle;
    private MainRobot mRobot;

    @Before
    public void setUp() throws Exception {
        mNewNoteTitle = mContext.getString(R.string.new_note);
        mEditNoteTitle = mContext.getString(R.string.edit_note);
        mRobot = new MainRobot(mContext);
    }

    @After
    public void tearDown() throws Exception {
        mContext.deleteDatabase(DbHelper.DB_NAME);
    }

    @Test
    public void addNewNote_noteWasWrong() throws Exception {
        mRobot.addNewNote()
                .checkToolbarTitle(mNewNoteTitle)
                .clickSave()
                .checkEmptyError();
    }

    @Test
    public void addNotes_notesWereAdded() throws Exception {
        for (String mDummyNote : mDummyNotes) {
            mRobot.addNewNote()
                    .checkToolbarTitle(mNewNoteTitle)
                    .fillNote(mDummyNote)
                    .clickSave();
        }

        mRobot.refreshNotes();

        int position = 0;
        long id = mDummyNotes.length;
        while (position < mDummyNotes.length) {
            mRobot.checkNoteIdAtPosition(position, id);
            position++;
            id--;
        }
    }

    @Test
    public void addNewNote_noteWasAdded() throws Exception {
        mRobot.addNewNote()
                .checkToolbarTitle(mNewNoteTitle)
                .fillNote(mDummyNotes[0])
                .clickSave();
        mRobot.checkNoteIdAtPosition(FIRST_NOTE_POSITION, FIRST_NOTE_ID);
    }

    @Test
    public void editNote_noteWasEdited() throws Exception {
        mRobot.addNewNote()
                .checkToolbarTitle(mNewNoteTitle)
                .fillNote(mDummyNotes[0])
                .clickSave()
                .checkNoteIdAtPosition(FIRST_NOTE_POSITION, FIRST_NOTE_ID)
                .selectNoteByPosition(FIRST_NOTE_POSITION)
                .checkToolbarTitle(mEditNoteTitle)
                .editNote(mDummyNotes[1])
                .clickSave()
                .checkNoteIdAtPosition(FIRST_NOTE_POSITION, FIRST_NOTE_ID);
    }

    @Test
    public void deleteNote_deleteCanceled() throws Exception {
        mRobot.addNewNote()
                .checkToolbarTitle(mNewNoteTitle)
                .fillNote(mDummyNotes[0])
                .clickSave()
                .checkNoteIdAtPosition(FIRST_NOTE_POSITION, FIRST_NOTE_ID)
                .selectNoteToBeDeletedByPosition(FIRST_NOTE_POSITION)
                .checkDeleteDialogIsPresent()
                .deleteNote();
        mRobot.checkNotesAreEmpty();
    }

    @Test
    public void deleteNote_noteWasNotDeleted() throws Exception {
        mRobot.addNewNote()
                .checkToolbarTitle(mNewNoteTitle)
                .fillNote(mDummyNotes[0])
                .clickSave()
                .checkNoteIdAtPosition(FIRST_NOTE_POSITION, FIRST_NOTE_ID)
                .selectNoteToBeDeletedByPosition(FIRST_NOTE_POSITION)
                .checkDeleteDialogIsPresent()
                .cancelDeleteNote();
        mRobot.checkNoteIdAtPosition(FIRST_NOTE_POSITION, FIRST_NOTE_ID);
    }
}
