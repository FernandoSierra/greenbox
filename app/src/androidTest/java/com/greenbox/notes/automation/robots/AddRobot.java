package com.greenbox.notes.automation.robots;

import android.content.Context;
import android.support.annotation.NonNull;

import com.greenbox.notes.R;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/11/16
 */

public class AddRobot extends BaseRobot {

    AddRobot(@NonNull Context context) {
        super(context);
    }

    @Override
    public AddRobot checkToolbarTitle(@NonNull String title) {
        return ((AddRobot) super.checkToolbarTitle(title));
    }

    public AddRobot fillNote(@NonNull String note) {
        onView(withId(R.id.edit_message))
                .perform(typeText(note));
        closeSoftKeyboard();
        return this;
    }

    public MainRobot clickSave() {
        onView(withId(R.id.btn_save))
                .perform(click());
        return new MainRobot(mContext);
    }

    public AddRobot editNote(String note) {
        onView(withId(R.id.edit_message))
                .perform(clearText(), typeText(note));
        closeSoftKeyboard();
        return this;
    }
}
