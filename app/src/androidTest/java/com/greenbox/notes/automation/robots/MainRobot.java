package com.greenbox.notes.automation.robots;

import android.content.Context;
import android.support.annotation.NonNull;

import com.greenbox.notes.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.greenbox.notes.automation.matchers.NotesMatcher.atPosition;
import static com.greenbox.notes.automation.matchers.NotesMatcher.isRecyclerViewEmpty;
import static com.greenbox.notes.automation.matchers.NotesMatcher.withNoteId;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/11/16
 */

public class MainRobot extends BaseRobot {

    public MainRobot(@NonNull Context context) {
        super(context);
    }

    public AddRobot addNewNote() {
        onView(withId(R.id.fab_add))
                .perform(click());
        return new AddRobot(mContext);
    }

    public MainRobot checkNoteIdAtPosition(int position, long noteId) {
        onView(withId(R.id.recycler_view))
                .check(matches(atPosition(position, withNoteId(noteId))));
        return this;
    }

    public AddRobot selectNoteByPosition(int position) {
        onView(withId(R.id.recycler_view))
                .perform(actionOnItemAtPosition(position, click()));
        return new AddRobot(mContext);
    }

    public MainRobot selectNoteToBeDeletedByPosition(int position) {
        onView(withId(R.id.recycler_view))
                .perform(actionOnItemAtPosition(position, longClick()));
        return this;
    }

    public MainRobot checkNotesAreEmpty() {
        onView(withId(R.id.recycler_view))
                .check(matches(isRecyclerViewEmpty()));
        return this;
    }

    public MainRobot refreshNotes() {
        onView(withId(R.id.recycler_view))
                .perform(swipeDown());
        return this;
    }
}
