package com.greenbox.notes.automation.matchers;

import android.support.annotation.NonNull;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 19/10/16
 */

public class NotesMatcher {

    private NotesMatcher() {
    }

    public static Matcher<View> withToolbarTitle(final CharSequence title) {
        return new BoundedMatcher<View, Toolbar>(Toolbar.class) {
            @Override
            protected boolean matchesSafely(Toolbar toolbar) {
                return title.equals(toolbar.getTitle());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with toolbar title: " + title);
            }
        };
    }

    public static Matcher<View> atPosition(final int position,
                                           @NonNull final Matcher<Object> matcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            protected boolean matchesSafely(RecyclerView recycler) {
                long noteId = recycler.getAdapter().getItemId(position);
                return matcher.matches(noteId);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                matcher.describeTo(description);
            }
        };
    }

    public static Matcher<View> isRecyclerViewEmpty() {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            protected boolean matchesSafely(RecyclerView item) {
                return item != null && (item.getAdapter().getItemCount() == 0);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("recycler view should be empty");
            }
        };
    }

    public static Matcher<Object> withNoteId(final Long id) {
        return new BoundedMatcher<Object, Long>(Long.class) {
            @Override
            protected boolean matchesSafely(Long noteId) {
                return id.equals(noteId);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with note id: " + id);
            }
        };
    }
}
