package com.greenbox.notes.view.application;

import android.app.Application;

import com.greenbox.notes.DependencyInjector;
import com.greenbox.notes.database.DbHelper;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/07/16
 */

public class NotesApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DependencyInjector.init(this);
        createDatabase();
    }

    private void createDatabase() {
        DbHelper dbHelper = new DbHelper(this);
        dbHelper.getWritableDatabase();
    }
}
