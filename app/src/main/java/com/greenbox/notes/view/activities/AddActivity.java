package com.greenbox.notes.view.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.greenbox.notes.Constants;
import com.greenbox.notes.DependencyInjector;
import com.greenbox.notes.R;
import com.greenbox.notes.model.Note;
import com.greenbox.notes.presentation.interactor.AddViewInteractor;
import com.greenbox.notes.presentation.presenter.AddPresenter;

public class AddActivity extends AppCompatActivity implements AddViewInteractor {
    private EditText mMessage;
    private View mContainer;
    private AddPresenter mPresenter;
    private Note mNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        mPresenter = DependencyInjector.getInstance().provideAddPresenter(this);
        mMessage = ((EditText) findViewById(R.id.edit_message));
        mContainer = findViewById(R.id.add_container);
        Button save = ((Button) findViewById(R.id.btn_save));
        mNote = getIntent().getParcelableExtra(Constants.Extras.NOTE);
        setupToolbar(mNote);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.saveOrUpdateNote(mNote);
            }
        });
    }

    private void setupToolbar(Note note) {
        String title;
        if (note == null) {
            title = getString(R.string.new_note);
        } else {
            title = getString(R.string.edit_note);
            mMessage.setText(note.getMessage());
        }
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Note extractViewDataToNote(long noteId) {
        String messageString = mMessage.getText().toString().trim();
        if (messageString.isEmpty()) {
            showEmptyMessageError();
            return null;
        } else {
            Note note = new Note();
            note.setId(noteId);
            note.setDate(System.currentTimeMillis());
            note.setMessage(messageString);
            return note;
        }
    }

    @Override
    public void showError() {
        Snackbar.make(mContainer, R.string.save_error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyMessageError() {
        Snackbar.make(mContainer, R.string.empty_note_error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void close() {
        finish();
    }
}
