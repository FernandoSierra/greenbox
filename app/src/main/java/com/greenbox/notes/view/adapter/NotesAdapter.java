package com.greenbox.notes.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.greenbox.notes.R;
import com.greenbox.notes.model.Note;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/07/16
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesHolder> {
    private final SimpleDateFormat mDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
    private LayoutInflater mLayoutInflater;
    private List<Note> mNotes = new ArrayList<>();
    private OnNoteClick mOnNoteClick;
    private OnNoteLongClick mOnNoteLongClick;

    public NotesAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setNotes(List<Note> notes) {
        mNotes = notes;
    }

    public void setOnNoteClick(OnNoteClick onNoteClick) {
        mOnNoteClick = onNoteClick;
    }

    public void setOnNoteLongClick(OnNoteLongClick onNoteLongClick) {
        mOnNoteLongClick = onNoteLongClick;
    }

    @Override
    public NotesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = mLayoutInflater.inflate(R.layout.note_item, parent, false);
        return new NotesHolder(root);
    }

    @Override
    public void onBindViewHolder(NotesHolder holder, int position) {
        final Note note = mNotes.get(position);
        holder.date.setText(mDateFormat.format(new Date(note.getDate())));
        holder.message.setText(note.getMessage());
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnNoteClick != null) {
                    mOnNoteClick.onClick(note);
                }
            }
        });
        holder.root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mOnNoteLongClick != null) {
                    mOnNoteLongClick.onLongClick(note);
                }
                return false;
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return mNotes.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public interface OnNoteClick {
        void onClick(Note note);
    }

    public interface OnNoteLongClick {
        void onLongClick(Note note);
    }

    static class NotesHolder extends RecyclerView.ViewHolder {
        final View root;
        final TextView date;
        final TextView message;

        NotesHolder(View itemView) {
            super(itemView);
            root = itemView;
            date = ((TextView) itemView.findViewById(R.id.lbl_date));
            message = ((TextView) itemView.findViewById(R.id.lbl_message));
        }
    }
}
