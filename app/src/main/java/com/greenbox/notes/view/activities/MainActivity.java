package com.greenbox.notes.view.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.greenbox.notes.Constants;
import com.greenbox.notes.DependencyInjector;
import com.greenbox.notes.R;
import com.greenbox.notes.model.Note;
import com.greenbox.notes.presentation.interactor.MainViewInteractor;
import com.greenbox.notes.presentation.presenter.MainPresenter;
import com.greenbox.notes.view.adapter.NotesAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainViewInteractor {
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private FloatingActionButton mActionButton;
    private View mEmptyView;
    private View mContainer;
    private NotesAdapter mAdapter;
    private MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = DependencyInjector.getInstance().provideMainPresenter(this);
        linkViews();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.refreshData();
    }

    @Override
    public void showRefresh() {
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void hideRefresh() {
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void update(List<Note> notes) {
        mAdapter.setNotes(notes);
        mAdapter.notifyDataSetChanged();
        int visibility;
        if (notes.isEmpty()) {
            visibility = View.VISIBLE;
        } else {
            visibility = View.GONE;
        }
        mEmptyView.setVisibility(visibility);
    }

    @Override
    public void showDeleteDialog(final long noteId) {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(R.string.delete_note))
                .setMessage(getString(R.string.delete_message))
                .setNegativeButton(getString(R.string.common_cancel), null)
                .setPositiveButton(getString(R.string.delete_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPresenter.deleteNote(noteId);
                    }
                }).show();
    }

    @Override
    public void showError() {
        Snackbar.make(mContainer, R.string.save_error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToAdd(Note note) {
        Intent intent = new Intent(MainActivity.this, AddActivity.class);
        if (note != null) {
            intent.putExtra(Constants.Extras.NOTE, note);
        }
        startActivity(intent);
    }

    private void linkViews() {
        mContainer = findViewById(R.id.coordinator_layout);
        mRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.refresh_layout));
        mRecyclerView = ((RecyclerView) findViewById(R.id.recycler_view));
        mActionButton = ((FloatingActionButton) findViewById(R.id.fab_add));
        mEmptyView = findViewById(R.id.lbl_empty);
    }

    private void initViews() {
        mAdapter = new NotesAdapter(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mRecyclerView.setAdapter(mAdapter);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.refreshData();
            }
        });
        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToAdd(null);
            }
        });
        mAdapter.setOnNoteClick(new NotesAdapter.OnNoteClick() {
            @Override
            public void onClick(Note note) {
                navigateToAdd(note);
            }
        });
        mAdapter.setOnNoteLongClick(new NotesAdapter.OnNoteLongClick() {
            @Override
            public void onLongClick(final Note note) {
                showDeleteDialog(note.getId());
            }
        });
    }
}
