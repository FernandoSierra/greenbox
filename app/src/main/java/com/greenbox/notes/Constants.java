package com.greenbox.notes;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/07/16
 */

public class Constants {
    public static final String TAG_LOG = "Notes";

    private Constants() {
        // nothing
    }

    public static class Extras {
        public static final String NOTE = "note";
    }
}
