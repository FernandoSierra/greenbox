package com.greenbox.notes.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.greenbox.notes.database.dao.NoteDao;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/07/16
 */

public class DbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "Notes.sqlite";
    private static final int DB_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(NoteDao.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // nothing
    }
}
