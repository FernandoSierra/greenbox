package com.greenbox.notes.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.provider.BaseColumns;

import com.greenbox.notes.database.DbHelper;
import com.greenbox.notes.model.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/07/16
 */

public class NoteDao implements BaseColumns {
    private static final String WHERE_BY_ID = _ID + "=?";
    private static final String TABLE_NAME = "Notes";
    private static final String DATE = "date";
    private static final String MESSAGE = "message";
    public static final String CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + _ID + "" +
            " INTEGER PRIMARY KEY, " + DATE + " INTEGER NOT NULL, " + MESSAGE + " " +
            "TEXT NOT NULL);";
    private static final String[] COLUMNS = {_ID, DATE, MESSAGE};
    private static final String ORDER_DATE = DATE + " DESC";
    private final DbHelper mDbHelper;

    public NoteDao(DbHelper dbHelper) {
        mDbHelper = dbHelper;
    }

    public List<Note> getNotes() throws SQLiteException {
        List<Note> notes = new ArrayList<>();
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        Cursor cursor;
        cursor = database.query(TABLE_NAME, COLUMNS, null, null, null, null, ORDER_DATE);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                notes.add(extractNote(cursor));
            }
            cursor.close();
        }
        database.close();
        return notes;
    }

    public boolean addNote(Note note) throws SQLiteException {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATE, note.getDate());
        contentValues.put(MESSAGE, note.getMessage());
        boolean wasInserted = database.insert(TABLE_NAME, null, contentValues) >= 0;
        database.close();
        return wasInserted;
    }

    public boolean updateNote(Note note) throws SQLiteException {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(_ID, note.getId());
        contentValues.put(DATE, note.getDate());
        contentValues.put(MESSAGE, note.getMessage());
        boolean wasUpdated = database.update(TABLE_NAME,
                contentValues,
                WHERE_BY_ID,
                new String[]{String.valueOf(note.getId())}) >= 0;
        database.close();
        return wasUpdated;
    }

    private Note extractNote(Cursor cursor) throws SQLiteException {
        Note note = new Note();
        note.setId(cursor.getLong(cursor.getColumnIndexOrThrow(_ID)));
        note.setDate(cursor.getLong(cursor.getColumnIndexOrThrow(DATE)));
        note.setMessage(cursor.getString(cursor.getColumnIndexOrThrow(MESSAGE)));
        return note;
    }

    public boolean deleteNote(long noteId) throws SQLiteException {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        boolean wasDeleted = database.delete(TABLE_NAME, WHERE_BY_ID,
                new String[]{String.valueOf(noteId)}) > 0;
        database.close();
        return wasDeleted;
    }
}
