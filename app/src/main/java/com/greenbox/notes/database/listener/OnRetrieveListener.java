package com.greenbox.notes.database.listener;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public interface OnRetrieveListener<T> {

    void onRetrieve(T item);

    void onError(Throwable t);
}
