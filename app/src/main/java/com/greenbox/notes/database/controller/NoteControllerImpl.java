package com.greenbox.notes.database.controller;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Handler;

import com.greenbox.notes.database.DbHelper;
import com.greenbox.notes.database.dao.NoteDao;
import com.greenbox.notes.database.listener.OnRetrieveListener;
import com.greenbox.notes.model.Note;

import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public class NoteControllerImpl implements NoteController {
    private final NoteDao mNoteDao;
    private final Handler mHandler;

    public NoteControllerImpl(Context context) {
        mNoteDao = new NoteDao(new DbHelper(context));
        mHandler = new Handler(context.getMainLooper());
    }

    @Override
    public void getAll(final OnRetrieveListener<List<Note>> onRetrieveListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final List<Note> notes = mNoteDao.getNotes();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onRetrieve(notes);
                        }
                    });
                } catch (final SQLiteException e) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onError(e);
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void add(final Note note, final OnRetrieveListener<Boolean> onRetrieveListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final boolean result = mNoteDao.addNote(note);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onRetrieve(result);
                        }
                    });
                } catch (final SQLiteException e) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onError(e);
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void update(final Note note, final OnRetrieveListener<Boolean> onRetrieveListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final boolean result = mNoteDao.updateNote(note);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onRetrieve(result);
                        }
                    });
                } catch (final SQLiteException e) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onError(e);
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void delete(final long noteId, final OnRetrieveListener<Boolean> onRetrieveListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final boolean result = mNoteDao.deleteNote(noteId);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onRetrieve(result);
                        }
                    });
                } catch (final SQLiteException e) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onRetrieveListener.onError(e);
                        }
                    });
                }
            }
        }).start();
    }
}
