package com.greenbox.notes.database.controller;

import com.greenbox.notes.database.listener.OnRetrieveListener;
import com.greenbox.notes.model.Note;

import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public interface NoteController {

    void getAll(OnRetrieveListener<List<Note>> onRetrieveListener);

    void add(Note note, OnRetrieveListener<Boolean> onRetrieveListener);

    void update(Note note, OnRetrieveListener<Boolean> onRetrieveListener);

    void delete(long noteId, OnRetrieveListener<Boolean> onRetrieveListener);
}
