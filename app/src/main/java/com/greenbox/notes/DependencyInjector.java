package com.greenbox.notes;

import android.content.Context;

import com.greenbox.notes.database.controller.NoteController;
import com.greenbox.notes.database.controller.NoteControllerImpl;
import com.greenbox.notes.presentation.interactor.AddViewInteractor;
import com.greenbox.notes.presentation.interactor.MainViewInteractor;
import com.greenbox.notes.presentation.presenter.AddPresenter;
import com.greenbox.notes.presentation.presenter.AddPresenterImpl;
import com.greenbox.notes.presentation.presenter.MainPresenter;
import com.greenbox.notes.presentation.presenter.MainPresenterImpl;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public class DependencyInjector {
    private static DependencyInjector sInstance;
    private Context mContext;

    public static void init(Context context) {
        sInstance = new DependencyInjector();
        sInstance.mContext = context;
    }

    public static DependencyInjector getInstance() {
        if (sInstance == null) {
            sInstance = new DependencyInjector();
        }
        return sInstance;
    }

    public MainPresenter provideMainPresenter(MainViewInteractor mainViewInteractor) {
        return new MainPresenterImpl(mainViewInteractor, provideNoteController());
    }

    public NoteController provideNoteController() {
        return new NoteControllerImpl(mContext);
    }

    public AddPresenter provideAddPresenter(AddViewInteractor addViewInteractor) {
        return new AddPresenterImpl(addViewInteractor, provideNoteController());
    }
}
