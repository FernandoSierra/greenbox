package com.greenbox.notes.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/07/16
 */

public class Note implements Parcelable {
    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    private Long mId;
    private Long mDate;
    private String mMessage;

    protected Note(Parcel in) {
        mId = in.readLong();
        mDate = in.readLong();
        mMessage = in.readString();
    }

    public Note() {
        // nothing
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeLong(mDate);
        dest.writeString(mMessage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Note note = (Note) o;

        if (mId != note.mId) return false;
        if (mDate != note.mDate) return false;
        return mMessage.equals(note.mMessage);

    }

    @Override
    public int hashCode() {
        int result = (int) (mId ^ (mId >>> 32));
        result = 31 * result + (int) (mDate ^ (mDate >>> 32));
        result = 31 * result + mMessage.hashCode();
        return result;
    }
}
