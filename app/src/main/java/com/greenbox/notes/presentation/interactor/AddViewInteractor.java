package com.greenbox.notes.presentation.interactor;

import com.greenbox.notes.model.Note;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public interface AddViewInteractor {

    Note extractViewDataToNote(long noteId);

    void showError();

    void showEmptyMessageError();

    void close();
}
