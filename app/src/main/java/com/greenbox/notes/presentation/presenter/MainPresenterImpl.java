package com.greenbox.notes.presentation.presenter;

import android.util.Log;

import com.greenbox.notes.Constants;
import com.greenbox.notes.database.controller.NoteController;
import com.greenbox.notes.database.listener.OnRetrieveListener;
import com.greenbox.notes.model.Note;
import com.greenbox.notes.presentation.interactor.MainViewInteractor;

import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public class MainPresenterImpl implements MainPresenter {
    private final MainViewInteractor mMainViewInteractor;
    private final NoteController mNoteController;

    public MainPresenterImpl(MainViewInteractor mainViewInteractor, NoteController noteController) {
        mMainViewInteractor = mainViewInteractor;
        mNoteController = noteController;
    }

    @Override
    public void refreshData() {
        mMainViewInteractor.showRefresh();
        mNoteController.getAll(new OnRetrieveListener<List<Note>>() {
            @Override
            public void onRetrieve(List<Note> item) {
                mMainViewInteractor.update(item);
                mMainViewInteractor.hideRefresh();
            }

            @Override
            public void onError(Throwable t) {
                Log.e(Constants.TAG_LOG, t.getMessage(), t);
                mMainViewInteractor.showError();
                mMainViewInteractor.hideRefresh();
            }
        });
    }

    @Override
    public void deleteNote(long noteId) {
        mNoteController.delete(noteId, new OnRetrieveListener<Boolean>() {
            @Override
            public void onRetrieve(Boolean item) {
                if (item) {
                    refreshData();
                } else {
                    mMainViewInteractor.showError();
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.e(Constants.TAG_LOG, t.getMessage(), t);
                mMainViewInteractor.showError();
            }
        });
    }
}
