package com.greenbox.notes.presentation.interactor;

import com.greenbox.notes.model.Note;

import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public interface MainViewInteractor {

    void showRefresh();

    void hideRefresh();

    void update(List<Note> notes);

    void showDeleteDialog(long noteId);

    void showError();

    void navigateToAdd(Note note);
}
