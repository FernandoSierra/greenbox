package com.greenbox.notes.presentation.presenter;

import android.util.Log;

import com.greenbox.notes.Constants;
import com.greenbox.notes.database.controller.NoteController;
import com.greenbox.notes.database.listener.OnRetrieveListener;
import com.greenbox.notes.model.Note;
import com.greenbox.notes.presentation.interactor.AddViewInteractor;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public class AddPresenterImpl implements AddPresenter, OnRetrieveListener<Boolean> {
    private final AddViewInteractor mAddViewInteractor;
    private final NoteController mNoteController;

    public AddPresenterImpl(AddViewInteractor addViewInteractor, NoteController noteController) {
        mAddViewInteractor = addViewInteractor;
        mNoteController = noteController;
    }

    @Override
    public void saveOrUpdateNote(Note note) {
        Note filledNote = mAddViewInteractor.extractViewDataToNote(note == null ? 0 : note.getId());
        if (filledNote != null) {
            if (filledNote.getId() == 0) {
                mNoteController.add(filledNote, this);
            } else {
                mNoteController.update(filledNote, this);
            }
        }
    }

    @Override
    public void onRetrieve(Boolean item) {
        if (item) {
            mAddViewInteractor.close();
        } else {
            mAddViewInteractor.showError();
        }
    }

    @Override
    public void onError(Throwable t) {
        Log.e(Constants.TAG_LOG, t.getMessage(), t);
        mAddViewInteractor.showError();
    }
}
