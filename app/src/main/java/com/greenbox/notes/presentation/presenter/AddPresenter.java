package com.greenbox.notes.presentation.presenter;

import com.greenbox.notes.model.Note;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public interface AddPresenter {

    void saveOrUpdateNote(Note note);
}
