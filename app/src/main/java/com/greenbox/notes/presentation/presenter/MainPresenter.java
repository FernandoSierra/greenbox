package com.greenbox.notes.presentation.presenter;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 18/07/16
 */

public interface MainPresenter {

    void refreshData();

    void deleteNote(long noteId);
}
